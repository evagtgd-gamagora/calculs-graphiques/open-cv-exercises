﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Emgu.CV;
using Emgu.CV.Util;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Drawing;
using System.IO;

public class Face : MonoBehaviour
{
    [Header("Unity display")]
    public UnityEngine.UI.Image _ui_display;
    public UnityEngine.UI.Image _ui_display_2;
    private Texture2D _tex_display;
    private Vector2 _facePositionOnImage;

    [Header("Webcam and window settings")]
    public int _webcamIdNumber = 0;
    private string _window = "Faces detection";
    private VideoCapture _webcam;
    private Mat _webcamFrame;
    private Mat _frameDisplay;
    private Mat _frameGray;

    [Header("Face detection settings")]
    public CascadeClassifier _frontFacesCascadeClassifier;
    private string _absolutePathToFrontFacesCascadeClassifier = "C:/Users/egerbert-g/eva-workspace/pgm/open-cv/emgu-cv-ball/Assets/Emgu-CV/data/lbpcascades/lbpcascade_frontalface.xml";

    private Rectangle[] _frontFaces;
    private int MIN_FACE_SIZE = 50;
    private int MAX_FACE_SIZE = 300;

    private void Start()
    {
        _webcam = new VideoCapture(_webcamIdNumber);
        _webcamFrame = new Mat();
        _frameDisplay = new Mat();
        _frameGray = new Mat();
        _frontFacesCascadeClassifier = new CascadeClassifier(fileName: _absolutePathToFrontFacesCascadeClassifier);


        //_webcam.Start();
        //_webcam.ImageGrabbed += _handleWebcamQueryFrame;
        _webcam.ImageGrabbed += new EventHandler(_handleWebcamQueryFrame);
    }

    private void OnDestroy()
    {
        _webcam.Stop();
        _webcam.Dispose();
        CvInvoke.DestroyAllWindows();
    }

    private void Update()
    {
        if (_webcam.IsOpened)
        {
            _webcam.Grab();
        }

        _displayFrame();
        

    }

    private void _displayFrame()
    {
        if(!_frameDisplay.IsEmpty)
        {
            Destroy(_tex_display);
            _tex_display = convertMatToTexture2D(_frameDisplay.Clone(),
                (int)_ui_display.rectTransform.rect.width,
                (int)_ui_display.rectTransform.rect.height);

            _ui_display.sprite = Sprite.Create(_tex_display,
                new Rect(0.0f, 0.0f, _tex_display.width, _tex_display.height),
                new Vector2(0.5f, 0.5f),
                100.0f);

            Mat tmp;
            if (_frontFaces.Length > 0)
                tmp = new Mat(_frameDisplay, _frontFaces[0]).Clone();
            else
                tmp = _frameDisplay.Clone();

            CvInvoke.Flip(tmp, tmp, FlipType.Horizontal);
            _tex_display = convertMatToTexture2D(tmp,
                (int)_ui_display_2.rectTransform.rect.width,
                (int)_ui_display_2.rectTransform.rect.height);

            _ui_display_2.sprite = Sprite.Create(_tex_display,
                new Rect(0.0f, 0.0f, _tex_display.width, _tex_display.height),
                new Vector2(0.5f, 0.5f),
                100.0f);
        }
    }

    private Texture2D convertMatToTexture2D(Mat mat, int width, int height)
    {
        if (mat.IsEmpty) return new Texture2D(width, height);
        CvInvoke.Resize(mat, mat, new Size(width, height));

        if (mat.IsEmpty) return new Texture2D(width, height);
        CvInvoke.Flip(mat, mat, FlipType.Vertical);

        if (mat.IsEmpty) return new Texture2D(width, height);
        Texture2D texture = new Texture2D(width, height, TextureFormat.RGBA32, false);
        texture.LoadRawTextureData(mat.ToImage<Rgba, byte>().Bytes);
        texture.Apply();

        return texture;
    }

    private void _handleWebcamQueryFrame(object sender, EventArgs e)
    {
        if (_webcam.IsOpened) _webcam.Retrieve(_webcamFrame);
        if (_webcamFrame.IsEmpty) return;
        FrameProcessing();
    }

    private void FrameProcessing()
    {
        _frameDisplay = _webcamFrame.Clone();
        CvInvoke.Flip(_frameDisplay, _frameDisplay, FlipType.Horizontal);

        Mat _webcamFrameGray = _frameDisplay.Clone();

        CvInvoke.CvtColor(_webcamFrameGray, _webcamFrameGray, ColorConversion.Bgr2Gray);
        if (_webcamFrameGray.IsEmpty) return;

        _frontFaces = _frontFacesCascadeClassifier.DetectMultiScale(_webcamFrameGray, minSize: new Size(MIN_FACE_SIZE, MIN_FACE_SIZE), maxSize: new Size(MAX_FACE_SIZE, MAX_FACE_SIZE), minNeighbors: 5);

        for (int i = 0; i < _frontFaces.Length; i++)
        {
            CvInvoke.Rectangle(_frameDisplay, _frontFaces[i], new MCvScalar(0, 0, 255), thickness: 3);
        }

        if (!_frameDisplay.IsEmpty) CvInvoke.Imshow(_window, _frameDisplay);
    }    
}
