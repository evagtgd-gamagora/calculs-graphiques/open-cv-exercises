﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Emgu.CV;
using Emgu.CV.Util;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Drawing;
using System.IO;

public class SaberDetection : MonoBehaviour
{
    

    /// <summary>
    /// An abstraction for the webcam
    /// </summary>
    /// 
    [Header("Webcam settings")]
    public int _webcamIdNumber = 0;
    public VideoCapture _webcam;
    

    [Header("Frames and debug display")]
    private string _window_saber = "Saber detection window";
    private Mat _webcamFrame;
    private Mat _frameDisplay;
    private Mat _frameGray;


    /// <summary>
    /// Filters parameters
    /// </summary>
    /// 
    [Header("Filters settings")]
    public bool _activateMedianBlur;
    [Range(1, 10)]
    public int _medianBlurFactor;

    public bool _activateGaussianBlur;
    [Range(1, 10)]
    public int _gaussianBlurFactor;
    [Range(0.8f, 1.2f)]
    public float _gaussianBlurSigmaFactor;

    /// <summary>
    /// Color triggers
    /// </summary>
    [Header("Color triggers settings")]
    [Range(0, 255)]
    public double _hueLowTrigger;
    [Range(0, 255)]
    public double _hueHighTrigger;
    [Range(0, 255)]
    public double _saturationLowTrigger;
    [Range(0, 255)]
    public double _saturationHighTrigger;
    [Range(0, 255)]
    public double _valueLowTrigger;
    [Range(0, 255)]
    public double _valueHighTrigger;

    [Header("Morphology settings")]
    [Range(0, 10)]
    public int _morphingOperationSize;
    [Range(1, 10)]
    public int _morphingIteration;
    public bool _morphingOpening;
    public bool _morphingClosing;

    [Header("Contours settings")]
    public bool _enableContours;


    void Start()
    {
        _webcam = new VideoCapture(_webcamIdNumber);
        _webcamFrame = new Mat();
        _frameDisplay = new Mat();
        _frameGray = new Mat();
        _webcam.ImageGrabbed += new EventHandler(_handleWebcamQueryFrame);
    }

    private void OnDestroy()
    {
        _webcam.Stop();
        _webcam.Dispose();
        CvInvoke.DestroyAllWindows();
    }

    void Update()
    {
        if (_webcam.IsOpened)
            _webcam.Grab();
    }

    private void _handleWebcamQueryFrame(object sender, EventArgs e)
    {
        if (_webcam.IsOpened) _webcam.Retrieve(_webcamFrame);
        if (_webcamFrame.IsEmpty) return;
        FrameProcessing();
    }

    private void FrameProcessing()
    {
        Mat workingFrame = _webcamFrame.Clone();

        CvInvoke.Flip(workingFrame, workingFrame, FlipType.Horizontal);

        Mat imgHSV = workingFrame.Clone();

        //Convert in HSV format (Hue, Saturation, Lightness)
        CvInvoke.CvtColor(workingFrame, imgHSV, ColorConversion.Bgr2Hsv);
        imgHSV = filter(imgHSV);

        //Tresholding
        Image<Gray, byte> imageThreshold = thresholding(imgHSV.ToImage<Hsv, byte>());

        //Morphology
        Mat imgMorph = morphing(imageThreshold.Mat);

        //Contours
        Mat imgOutline = imgMorph.Clone();
        if (_enableContours)
        {
            VectorOfVectorOfPoint vectListOutline = contours(imgMorph);
        }

        //Display
        _frameDisplay = imgMorph.Clone();
        if (!_frameDisplay.IsEmpty) CvInvoke.Imshow(_window_saber, _frameDisplay);
    }

    private Image<Gray, byte> thresholding(Image<Hsv, byte> imageHSV)
    {
        //Tresholding
        Hsv hsvLowTrigger = new Hsv(_hueLowTrigger, _saturationLowTrigger, _valueLowTrigger);
        Hsv hsvHighTrigger = new Hsv(_hueHighTrigger, _saturationHighTrigger, _valueHighTrigger);

        Image<Gray, byte> imageThreshold = imageHSV.InRange(hsvLowTrigger, hsvHighTrigger);
        return imageThreshold;
    }

    private Mat filter(Mat img)
    {
        Mat imgFiltered = img.Clone();

        if (_activateMedianBlur)
            CvInvoke.MedianBlur(imgFiltered, imgFiltered, _medianBlurFactor * 2 + 1);

        if (_activateGaussianBlur)
        {
            int size = _gaussianBlurFactor * 2 + 1;
            CvInvoke.GaussianBlur(imgFiltered, imgFiltered, new Size(size, size), size * _gaussianBlurSigmaFactor);
        }

        return imgFiltered;
    }

    private Mat morphing(Mat img)
    {
        Mat imgMorph = img.Clone();
        //Mat structuringElement = CvInvoke.GetStructuringElement(ElementShape.Cross, new Size(_morphingOperationSize * 2 + 1, _morphingOperationSize * 2 + 1), new Point(-1, -1));
        Mat structuringElement = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new Size(_morphingOperationSize * 2 + 1, _morphingOperationSize * 2 + 1), new Point(-1, -1));

        if (_morphingOpening)
        {
            CvInvoke.Erode(imgMorph, imgMorph, structuringElement, new Point(-1, -1), _morphingIteration, BorderType.Constant, new MCvScalar(0));
            CvInvoke.Dilate(imgMorph, imgMorph, structuringElement, new Point(-1, -1), _morphingIteration, BorderType.Constant, new MCvScalar(0));
        }
        if (_morphingClosing)
        {
            CvInvoke.Dilate(imgMorph, imgMorph, structuringElement, new Point(-1, -1), _morphingIteration, BorderType.Constant, new MCvScalar(0));
            CvInvoke.Erode(imgMorph, imgMorph, structuringElement, new Point(-1, -1), _morphingIteration, BorderType.Constant, new MCvScalar(0));
        }

        return imgMorph;
    }

    private VectorOfVectorOfPoint contours(Mat img)
    {
        VectorOfVectorOfPoint vectListContours = new VectorOfVectorOfPoint();
        Mat hierarchy = new Mat();
        CvInvoke.FindContours(img, vectListContours, hierarchy, RetrType.List, ChainApproxMethod.ChainApproxNone);

        return vectListContours;
    }

    private List<RotatedRect> lightSaberRect(VectorOfVectorOfPoint contours)
    {
        List<RotatedRect> rectangles = new List<RotatedRect>();

        for (int i = 0; i < contours.Size; i++)
        {
            RotatedRect rect = CvInvoke.MinAreaRect(contours[i]);
            rectangles.Add(rect);
        }
        return rectangles;
    }
}
