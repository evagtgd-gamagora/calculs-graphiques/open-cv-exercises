﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Emgu.CV;
using Emgu.CV.Util;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Drawing;
using System.IO;

public class Ball : MonoBehaviour
{
    private string _window = "Webcam view";

    /// <summary>
    /// An abstraction for the webcam
    /// </summary>
    /// 
    [Header("Webacm and window settings")]
    public VideoCapture _webcam;

    public int _webcamIdNumber = 0;

    [Range(0.5f, 1.5f)]
    public float _resizeFactor;


    /// <summary>
    /// Filters parameters
    /// </summary>
    /// 
    [Header("Filters settings")]
    public bool _activateMedianBlur;
    [Range(1, 10)]
    public int _medianBlurFactor;

    public bool _activateMeanBlur;
    [Range(1, 10)]
    public int _meanBlurFactor;

    public bool _activateGaussianBlur;
    [Range(1, 10)]
    public int _gaussianBlurFactor;
    [Range(0.8f, 1.2f)]
    public float _gaussianBlurSigmaFactor;

    /// <summary>
    /// Color triggers
    /// </summary>
    [Header("Color triggers settings")]
    [Range(0, 255)]
    public double _hueLowTrigger;
    [Range(0, 255)]
    public double _hueHighTrigger;
    [Range(0, 255)]
    public double _saturationLowTrigger;
    [Range(0, 255)]
    public double _saturationHighTrigger;
    [Range(0, 255)]
    public double _valueLowTrigger;
    [Range(0, 255)]
    public double _valueHighTrigger;

    [Header("Morphology settings")]
    [Range(0, 10)]
    public int _morphingOperationSize;
    [Range(1, 10)]
    public int _morphingIteration;
    public bool _morphingOpening;
    public bool _morphingClosing;

    [Header("Outline settings")]
    public bool _outline;

    [Header("Display settings")]
    public bool _difference;


    void Start()
    {
        _webcam = new VideoCapture(_webcamIdNumber);
    }

    void Update()
    {
        Mat image;
        image = _webcam.QueryFrame();
        //Mat flippedImage = image.Clone();
        CvInvoke.Flip(image, image, FlipType.Horizontal);

        //_sizeFactor = 0.9f + Mathf.PingPong(0.2f * Time.time, 0.2f);
        
        CvInvoke.Resize(image, image, new Size(Mathf.CeilToInt(_resizeFactor * _webcam.Width), Mathf.CeilToInt(_resizeFactor * _webcam.Height)));

        Mat imgGray, imgHSV;

        imgGray = image.Clone();
        imgHSV = image.Clone();

        //Convert in grayscale format
        CvInvoke.CvtColor(image, imgGray, ColorConversion.Bgr2Gray);

        //Convert in HSV format (Hue, Saturation, Lightness)
        CvInvoke.CvtColor(image, imgHSV, ColorConversion.Bgr2Hsv);
        imgHSV = filter(imgHSV);

        //Tresholding
        Image<Hsv, byte> imageHSV = imgHSV.ToImage<Hsv, byte>();
        Image<Gray, byte> imageThreshold = thresholding(imageHSV);

        //Mat imgThreshold = imageThreshold.Mat.Clone();
        //CvInvoke.MedianBlur(imageThreshold.Mat, imgThreshold, _medianBlurFactor * 2 + 1);

        //Morphology
        Mat imgMorph = imageThreshold.Mat.Clone();
        Mat structuringElement = CvInvoke.GetStructuringElement(ElementShape.Cross, new Size(_morphingOperationSize * 2 + 1, _morphingOperationSize * 2 + 1), new Point(-1, -1));
        //Image<Gray, byte> imageMorph = imageThreshold.Dilate(1);
        if(_morphingOpening)
        {
            CvInvoke.Erode(imgMorph, imgMorph, structuringElement, new Point(-1, -1), _morphingIteration, BorderType.Constant, new MCvScalar(0));
            CvInvoke.Dilate(imgMorph, imgMorph, structuringElement, new Point(-1, -1), _morphingIteration, BorderType.Constant, new MCvScalar(0));
        }
        if (_morphingClosing)
        {
            CvInvoke.Dilate(imgMorph, imgMorph, structuringElement, new Point(-1, -1), _morphingIteration, BorderType.Constant, new MCvScalar(0));
            CvInvoke.Erode(imgMorph, imgMorph, structuringElement, new Point(-1, -1), _morphingIteration, BorderType.Constant, new MCvScalar(0));
        }

        //Outline
        Mat imgOutline = imgMorph.Clone();
        if (_outline)
        {
            VectorOfVectorOfPoint vectListOutline = new VectorOfVectorOfPoint();
            Mat hierarchy = new Mat();
            CvInvoke.FindContours(imgOutline, vectListOutline, hierarchy, RetrType.List, ChainApproxMethod.ChainApproxNone);

            int biggestOutline = -1;
            double biggestContourArea = 0;

            for (int i = 0; i < vectListOutline.Size; i++)
            {
                double contourArea = CvInvoke.ContourArea(vectListOutline[i], false);
                if (biggestContourArea < contourArea)
                {
                    biggestOutline = i;
                    biggestContourArea = contourArea;
                }
            }

            Point biggestOutlineCenter = new Point(0, 0);
            if (biggestOutline > -1)
            {
                //for (int i = 0; i < vectListOutline[biggestOutline].Size; i++)
                //{
                //    biggestOutlineCenter.X += vectListOutline[biggestOutline][i].X;
                //    biggestOutlineCenter.Y += vectListOutline[biggestOutline][i].Y;
                //}

                //biggestOutlineCenter.X /= vectListOutline[biggestOutline].Size;
                //biggestOutlineCenter.Y /= vectListOutline[biggestOutline].Size;
                var moments = CvInvoke.Moments(vectListOutline[biggestOutline]);
                int cx = (int)(moments.M10 / moments.M00);
                int cy = (int)(moments.M01 / moments.M00);
                biggestOutlineCenter = new Point(cx, cy);
            }

            CvInvoke.DrawContours(imgOutline, vectListOutline, biggestOutline, new MCvScalar(150), 3);

            CvInvoke.DrawContours(image, vectListOutline, biggestOutline, new MCvScalar(0,0,255), 3);
            if (biggestOutline > -1)
                CvInvoke.Circle(image, biggestOutlineCenter, 3, new MCvScalar(0, 0, 255), 2);
        }

        //Display
        Mat imgDisplay = image.Clone();
        Mat imgDifference = imgMorph.Clone();
        //CvInvoke.Subtract(imgDisplay, imgDifference, imgDifference);
        if(_difference)
            CvInvoke.Imshow(_window, imgDifference); //Slooowww
        else
            CvInvoke.Imshow(_window, imgDisplay); //Slooowww

        //Block Thread for 24 ms
        CvInvoke.WaitKey(24);
    }

    private void OnDestroy()
    {
        _webcam.Dispose();
        CvInvoke.DestroyAllWindows();
    }

    private Image<Gray, byte> thresholding(Image<Hsv, byte> imageHSV)
    {
        //Tresholding
        Hsv hsvLowTrigger = new Hsv(_hueLowTrigger, _saturationLowTrigger, _valueLowTrigger);
        Hsv hsvHighTrigger = new Hsv(_hueHighTrigger, _saturationHighTrigger, _valueHighTrigger);

        Image<Gray, byte> imageThreshold = imageHSV.InRange(hsvLowTrigger, hsvHighTrigger);
        return imageThreshold;
    }

    private Mat filter(Mat img)
    {
        Mat imgFiltered = img.Clone();

        if (_activateMeanBlur)
        {
            int size = _meanBlurFactor * 2 + 1;
            CvInvoke.Blur(imgFiltered, imgFiltered, new Size(size, size), new Point(-1, -1));
        }

        if (_activateMedianBlur)
            CvInvoke.MedianBlur(imgFiltered, imgFiltered, _medianBlurFactor * 2 + 1);

        if (_activateGaussianBlur)
        {
            int size = _gaussianBlurFactor * 2 + 1;
            CvInvoke.GaussianBlur(imgFiltered, imgFiltered, new Size(size, size), size * _gaussianBlurSigmaFactor);
        }

        return imgFiltered;
    }
}
