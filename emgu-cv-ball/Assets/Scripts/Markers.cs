﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Emgu.CV;
using Emgu.CV.Util;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Drawing;
using System.IO;

public class Markers : MonoBehaviour
{

    [Header("Webcam and window settings")]
    public int _webcamIdNumber = 0;
    private string _window = "Faces detection";
    private VideoCapture _webcam;
    private Mat _webcamFrame;
    private Mat _frameDisplay;
    private Mat _frameGray;

    [Header("Thresholding settings")]
    public bool _adaptive_threshold;
    public bool _adaptive_gaussian_threshold;
    [Range(0,255)]
    public double _threshold_value;
    [Range(0, 255)]
    public double _threshold_max_value;
    [Range(1, 10)]
    public int _threshold_bloc_size;
    [Range(-10, 10)]
    public double _threshold_param1;


    [Header("Filters settings")]
    public bool _activateMedianBlur;
    [Range(1, 10)]
    public int _medianBlurFactor;

    public bool _activateGaussianBlur;
    [Range(1, 10)]
    public int _gaussianBlurFactor;
    [Range(0.8f, 1.2f)]
    public float _gaussianBlurSigmaFactor;

    [Header("Morphology settings")]
    [Range(0, 10)]
    public int _morphingOperationSize;
    [Range(1, 10)]
    public int _morphingIteration;
    public bool _morphingOpening;
    public bool _morphingClosing;

    [Header("Contours settings")]
    public bool _enableContours;

    void Start()
    {
        _webcam = new VideoCapture(_webcamIdNumber);
        _webcamFrame = new Mat();
        _frameDisplay = new Mat();
        _frameGray = new Mat();
        _webcam.ImageGrabbed += new EventHandler(_handleWebcamQueryFrame);
    }

    // Update is called once per frame
    void Update()
    {
        if (_webcam.IsOpened)
        {
            _webcam.Grab();
        }
    }

    private void OnDestroy()
    {
        _webcam.Dispose();
        CvInvoke.DestroyAllWindows();
    }

    private void _handleWebcamQueryFrame(object sender, EventArgs e)
    {
        if (_webcam.IsOpened) _webcam.Retrieve(_webcamFrame);
        if (_webcamFrame.IsEmpty) return;
        FrameProcessing();
    }

    private void FrameProcessing()
    {
        _frameDisplay = _webcamFrame.Clone();
        CvInvoke.Flip(_frameDisplay, _frameDisplay, FlipType.Horizontal);

        Mat _webcamFrameGray = _frameDisplay.Clone();

        _webcamFrameGray = filter(_webcamFrameGray);

        CvInvoke.CvtColor(_webcamFrameGray, _webcamFrameGray, ColorConversion.Bgr2Gray);
        if (_webcamFrameGray.IsEmpty) return;

        if (_adaptive_threshold)
            if(_adaptive_gaussian_threshold)
                CvInvoke.AdaptiveThreshold(_webcamFrameGray, _webcamFrameGray, _threshold_max_value, AdaptiveThresholdType.GaussianC, ThresholdType.BinaryInv, 2 * _threshold_bloc_size + 1,_threshold_param1);
            else
                CvInvoke.AdaptiveThreshold(_webcamFrameGray, _webcamFrameGray, _threshold_max_value, AdaptiveThresholdType.MeanC, ThresholdType.BinaryInv, 2 * _threshold_bloc_size + 1, _threshold_param1);
        else
            CvInvoke.Threshold(_webcamFrameGray, _webcamFrameGray, _threshold_value, _threshold_max_value, ThresholdType.BinaryInv);

        _webcamFrameGray = morphing(_webcamFrameGray);

        if(_enableContours)
        {
            VectorOfVectorOfPoint contourList = contours(_webcamFrameGray);

            contourList = findMarkers(contourList);

            for (int i = 0; i < contourList.Size; i++)
            {
                CvInvoke.DrawContours(_webcamFrameGray, contourList, i, new MCvScalar(150), 2);
            }
        }
        

        _frameDisplay = _webcamFrameGray.Clone();

        if (!_frameDisplay.IsEmpty) CvInvoke.Imshow(_window, _frameDisplay);
    }

    private Mat filter(Mat img)
    {
        Mat imgFiltered = img.Clone();

        if (_activateMedianBlur)
            CvInvoke.MedianBlur(imgFiltered, imgFiltered, _medianBlurFactor * 2 + 1);

        if (_activateGaussianBlur)
        {
            int size = _gaussianBlurFactor * 2 + 1;
            CvInvoke.GaussianBlur(imgFiltered, imgFiltered, new Size(size, size), size * _gaussianBlurSigmaFactor);
        }

        return imgFiltered;
    }

    private Mat morphing(Mat img)
    {
        //Morphology
        Mat imgMorph = img.Clone();
        Mat structuringElement = CvInvoke.GetStructuringElement(ElementShape.Cross, new Size(_morphingOperationSize * 2 + 1, _morphingOperationSize * 2 + 1), new Point(-1, -1));
        //Image<Gray, byte> imageMorph = imageThreshold.Dilate(1);
        if (_morphingOpening)
        {
            CvInvoke.Erode(imgMorph, imgMorph, structuringElement, new Point(-1, -1), _morphingIteration, BorderType.Constant, new MCvScalar(0));
            CvInvoke.Dilate(imgMorph, imgMorph, structuringElement, new Point(-1, -1), _morphingIteration, BorderType.Constant, new MCvScalar(0));
        }
        if (_morphingClosing)
        {
            CvInvoke.Dilate(imgMorph, imgMorph, structuringElement, new Point(-1, -1), _morphingIteration, BorderType.Constant, new MCvScalar(0));
            CvInvoke.Erode(imgMorph, imgMorph, structuringElement, new Point(-1, -1), _morphingIteration, BorderType.Constant, new MCvScalar(0));
        }

        return imgMorph;
    }

    private VectorOfVectorOfPoint contours (Mat img)
    {
        VectorOfVectorOfPoint vectListContours = new VectorOfVectorOfPoint();
        Mat hierarchy = new Mat();
        CvInvoke.FindContours(img, vectListContours, hierarchy, RetrType.List, ChainApproxMethod.ChainApproxNone);

        return vectListContours;        
    }

    //TODO !!!
    private VectorOfVectorOfPoint findMarkers (VectorOfVectorOfPoint contours)
    {
        VectorOfVectorOfPoint vectListMarkers = new VectorOfVectorOfPoint();
        

        for (int i = 0; i < contours.Size; i++)
        {
            double contourArea = CvInvoke.ContourArea(contours[i], false);

            double permiter = CvInvoke.ArcLength(contours[i], true);

            bool convex = CvInvoke.IsContourConvex(contours[i]);

            var moments = CvInvoke.Moments(contours[i]);
            int cx = (int)(moments.M10 / moments.M00);
            int cy = (int)(moments.M01 / moments.M00);
            Point center = new Point(cx, cy);

            RotatedRect rect = CvInvoke.MinAreaRect(contours[i]);

            //CONDITION ???
            vectListMarkers.Push(contours[i]);
        }

        return vectListMarkers;
    }
}
